import 'reflect-metadata';
import { startServer } from './app';
import { conectar } from './config/typeorm';
/**
 * 
 * da inicio al server
 * 
 * 
 */
const main = async () => {
    const app = await startServer();
    const db = await conectar();
    app.listen(3000, () => {
        console.log('Servidor puerto 3000');
    });
}

// inicio el server
main();