import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { PhotoResolver } from './graphql/index.resolver';
import { buildSchema } from 'type-graphql';
const app = express();

/**
 * 
 * context define los tipos de respuestas del servidor 
 * resolvers inyecta los parametros de consulta y respuesta de la app
 * 
 * se inicia en una instancia de express 
 */
const startServer = async () => {
    const server = new ApolloServer({
        schema: await buildSchema({
            resolvers: [PhotoResolver],
        }),
        context: ({ req, res }) => ({ req, res })
    })
    server.applyMiddleware(
        { app, path: '/graphql' }
    )
    return app;
}

export {
    startServer
}