import { Resolver, Mutation, Query, Arg, Int, createUnionType } from 'type-graphql';
import { PhotoSchema } from '../schema/photometadata.schema';
import { MetadataSchema } from '../schema/photometadata.schema';
import { PhotoEntity } from '../../entity/photo.entity';
import { PhotoValidArgs } from '../validations/photo.validations';
import { PhotoMetadataInput } from '../clases/photometadata.input';
/* import { PhotoMetadataSchema } from '../schema/photometadata.schema'; */

@Resolver()
export class PhotoResolver {

    @Mutation(() => PhotoSchema)
    cPhoto(@Arg('args', () => PhotoMetadataInput) args: PhotoMetadataInput) {
        console.log('inicia mutacion');
        console.log(args);
        return PhotoValidArgs(args);
    }

    @Query(() => [PhotoSchema])
    async Photo() {
        return await PhotoEntity.find();
    }
}