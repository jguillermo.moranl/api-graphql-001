import { PhotoEntity } from '../../entity/photo.entity';
import { MetadataEntity } from '../../entity/metadata.entity';
import { PhotoMetadataInput } from '../clases/photometadata.input';
import { Transaction, TransactionManager, EntityManager } from 'typeorm';

export class ServiceORM {


    @Transaction()
    async SavemyAlbums(@TransactionManager() manager: EntityManager, args: any) {
        console.log({ args });
        const { Metadata, Photo } = args;
        console.log({ Photo });

        const photo = await PhotoEntity.create(Photo);
        const metadata = await MetadataEntity.create({
            ...Metadata, photo
        });
        const savePhoto = await manager.save(photo);
        const saveMetadata = await manager.save(metadata)
        console.log({ savePhoto, saveMetadata });
        return savePhoto;
    }


}
























/* const crudAll = async (args: PhotoMetadataInput) => {
    try {
        const { Metadata, Photo } = args;
        return await getConnection()
            .transaction("SERIALIZABLE", async transaccionManager => {
                const photo = await PhotoEntity.create(Photo);
                const metadata = await MetadataEntity.create({
                    ...Metadata, photo
                });
                transaccionManager.save(photo);
                transaccionManager.save(metadata);
                console.log('TRANSACCION REALIZADA');

            });

    } catch (error) {
        console.log(error);
    }
};

return { photo, metadata };
export { crudAll } */