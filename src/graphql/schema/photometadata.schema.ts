
import { ObjectType, Field, Int } from "type-graphql";

@ObjectType('PhotoSchema')
export class PhotoSchema {

    @Field(type => Int)
    id!: number;

    @Field()
    name!: string;

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    filename?: string;

    @Field({ nullable: true })
    views?: number;

    @Field({ nullable: true })
    isPublished?: boolean;
}

@ObjectType('MetadataSchema')
export class MetadataSchema {
    @Field()
    id!: number;

    @Field(type => Int)
    height!: number;

    @Field(type => Int)
    width!: number;

    @Field()
    orientation!: string;

    @Field()
    compressed!: boolean;

    @Field({ nullable: true })
    comment?: string;

    @Field(type => PhotoSchema)
    photo!: PhotoSchema;
}
/* 
@ObjectType('PhotoMetadataSchema')
export class PhotoMetadataSchema {
    @Field(type => PhotoSchema)
    photo!: PhotoSchema;
    @Field(type => MetadataSchema)
    metadata!: MetadataSchema;
} */