
import { UserInputError } from "apollo-server-express";

/* import { crudAll } from "../services/photo.service"; */
import { ServiceORM } from "../services/photo.service";
import { PhotoMetadataInput } from "../clases/photometadata.input";
import { getManager } from 'typeorm'


const PhotoValidArgs = async (args: PhotoMetadataInput) => {

    const service = new ServiceORM();

    const { Photo, Metadata } = args;
    if (Photo.name === '' || Photo.name === null || Photo.name === undefined) {
        throw new UserInputError('Args name on Photo is invalid')
    }
    if (Metadata.width === null || Metadata.width === undefined) {
        throw new UserInputError('Args width on Metadata is invalid')
    }
    if (Metadata.height === null || Metadata.height === undefined) {
        throw new UserInputError('Args height on Metadata is invalid')
    }
    if (Metadata.orientation === '' || Metadata.orientation === null || Metadata.orientation === undefined) {
        throw new UserInputError('Args orientation on Metadata is invalid')
    }
    if (Metadata.compressed === null || Metadata.compressed === undefined) {
        throw new UserInputError('Args compressed on Metadata is invalid')
    }
    console.log({ Photo });
    return await service.SavemyAlbums(getManager(), args);
}

export { PhotoValidArgs }