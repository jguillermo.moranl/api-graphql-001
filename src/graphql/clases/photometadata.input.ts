import { Field, Int, InputType } from 'type-graphql';

@InputType()
class PhotoInput {
    @Field()
    name!: string;
    @Field({ nullable: true })
    description?: string;
    @Field({ nullable: true })
    filename?: string;
}

@InputType()
class MetadataInput {
    @Field(type => Int)
    height!: number;

    @Field(type => Int)
    width!: number;

    @Field()
    orientation!: string;

    @Field()
    compressed!: boolean;

    @Field({ nullable: true })
    comment?: string;
}

@InputType()
export class PhotoMetadataInput {
    @Field()
    Photo!: PhotoInput;
    @Field()
    Metadata!: MetadataInput;
}

