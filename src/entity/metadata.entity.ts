import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, BaseEntity } from "typeorm";
import { PhotoEntity } from "./photo.entity";

@Entity('metadata')
export class MetadataEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column("int")
    height!: number;

    @Column("int")
    width!: number;

    @Column({ default: 'horizontal' })
    orientation!: string;

    @Column({ default: false })
    compressed!: boolean;

    @Column({ nullable: true })
    comment?: string;

    @OneToOne(type => PhotoEntity, { lazy: true })
    @JoinColumn({ name: 'photo', referencedColumnName: 'id' })
    photo!: PhotoEntity;
}