import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from "typeorm";

@Entity('photo')
export class PhotoEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        length: 100
    })
    name!: string;

    @Column("text", { nullable: true })
    description?: string;

    @Column("varchar", { length: 50 })
    filename!: string;

    @Column("int", { nullable: true, default: 0 })
    views?: number;

    @Column({ nullable: true, default: false, type: 'bool' })
    isPublished?: boolean;
}