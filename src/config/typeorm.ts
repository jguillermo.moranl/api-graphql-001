import { createConnection } from 'typeorm';
import path from 'path';
const conectar = async () => {
    await createConnection({
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'dota2memo',
        database: 'albums',
        entities: [path.join(__dirname, '../entity/**/**.ts')], // en el caso de que el archivo final en build sea .js este debe llevar el .js
        synchronize: true
    });
    console.log('BASE DE DATOS CONECTADA');
}

export {
    conectar
}